﻿using Sitecore.Analytics.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Analytics.Outcome.Extensions;
using Sitecore.Analytics;

namespace events.tac.local.Controllers
{
    public class SubscribeFormController : Controller
    {
        // GET: SubscribeForm
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string email)
        {
            Sitecore.Analytics.Tracker.Current.Session.Identify(email);
            var contact = Sitecore.Analytics.Tracker.Current.Contact;
            var emails = contact.GetFacet<IContactEmailAddresses>("Emails");

            if (!emails.Entries.Contains("personal"))
            {
                emails.Preferred = "personal";
                var personalEmail = emails.Entries.Create("personal");
                personalEmail.SmtpAddress = email;
            }

            var outcome = new Sitecore.Analytics.Outcome.Model.ContactOutcome(Sitecore.Data.ID.NewID, (new Sitecore.Data.ID("{26936599-3208-493A-B554-4075BB71EEE0}")), new Sitecore.Data.ID(Tracker.Current.Contact.ContactId));
            Tracker.Current.RegisterContactOutcome(outcome);


            return View("Confirmation");
        }
    }
}