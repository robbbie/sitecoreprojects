﻿function createCommentItem(form, path) {
    var service = new ItemService({ url: '/sitecore/api/ssc/item' });
    var obj = {
        ItemName: 'comment - ' + form.value.name,
        TemplateID: '{F4F00458-A2BF-497D-9951-3095E78F98EB}',
        Name: form.name.value,
        Comment: form.comment.value
    };

    service.create(obj)
    .path(path)
    .execute()
    .then(function (item) {
        form.name.value = form.comment.value = '';
        window.alert('Thanks. Your message will show on the site shortly');
    })
    .fail(function (err) {
        window.alert(err);
    });

    event.preventDefault();
    return false;
}